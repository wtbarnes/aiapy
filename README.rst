=========
``aiapy``
=========

``aiapy`` is a Python package for analyzing data from the Atmospheric Imaging Assembly (AIA) instrument onboard NASA's Solar Dynamics Observatory (SDO) spacecraft.
For more information, see the `aiapy documentation <https://aiapy.readthedocs.io/en/latest/>`__.
For some examples of using aiapy, see `our gallery <https://aiapy.readthedocs.io/en/latest/generated/gallery/index.html>`__.

Citing
------

If you use ``aiapy`` in your scientific work, we would appreciate you citing it in your publications.
The latest citation information can be found in the `documentation <https://aiapy.readthedocs.io/en/latest/about.html>`__, or obtained with ``aiapy.__citation__``.

Contributing
------------

``aiapy`` is open source software, built on top of other open source software, and we'd love to have you contribute

Please checkout the `contributing guide <https://aiapy.readthedocs.io/en/latest/develop.html>`__ for more information on how to contribute to aiapy.
